﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 5f;
    private float dartSpeed = 30f;
    private float movement = 0f;
    private float jumpPressed = 0;
    private Rigidbody2D rigidBody;
    public Camera MainCamera;
    private Vector2 screenBounds;
    private float objectWidth;
    private float objectHeight;
    private bool isdartingForward = false;
    private bool isdartingBackwards = false;
    public bool isDead = false;
    private GameController gameController;

    private Rect topLeftSide;
    private Rect bottomLeftSide;
    private Rect rightSide;


    // Use this for initialization
    void Start()
    {
        screenBounds = MainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, MainCamera.transform.position.z));
        objectWidth = transform.GetComponent<SpriteRenderer>().bounds.extents.x; //extents = size of width / 2
        objectHeight = transform.GetComponent<SpriteRenderer>().bounds.extents.y; //extents = size of height / 2
        rigidBody = GetComponent<Rigidbody2D>();
        transform.position = new Vector3 (screenBounds.x * -1 + (objectWidth * 2), transform.position.y);
        gameController = FindObjectOfType<GameController>();

    }



    // Update is called once per frame
    void FixedUpdate()
    {


#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR
            movement = Input.GetAxis("Vertical");
            jumpPressed = Input.GetAxis("Jump");
        
        
#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE

            if (Input.touchCount > 0)
            {
                //Store the first touch detected.
                Touch myTouch = Input.touches[0];

                //Check if the phase of that touch equals Began
                if (myTouch.phase == TouchPhase.Began)
                {
                    Debug.Log("position: " + myTouch.position.y);
                    if (myTouch.position.y >(Screen.height/2) && myTouch.position.x < Screen.width/2){
                        movement = 1f;
                    } else if (myTouch.position.y <(Screen.height/2) && myTouch.position.x < Screen.width/2) {
                       movement = -1f;
                    } else {
                       jumpPressed = 1;
                    }
                }

                //If the touch phase is not Began, and instead is equal to Ended:
                else if (myTouch.phase == TouchPhase.Ended)
                {
                    movement = 0;
                    jumpPressed = 0;
                }
             }
#endif


        if (movement > 0f)
        {
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, movement * speed);
        }
        else if (movement < 0f)
        {
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, movement * speed);
        }
        else
        {
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, 0);
        }

        if (jumpPressed > 0 && !isdartingBackwards)
        {
            isdartingForward = true;
        }
        if(isdartingForward)
        {
            // dart forward until the player finds food or the end of the screen
            rigidBody.velocity = new Vector2(dartSpeed, rigidBody.velocity.y);
        } else if (isdartingBackwards)
        {
            rigidBody.velocity = new Vector2(-dartSpeed, rigidBody.velocity.y);
        }

    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 viewPos = transform.position;
        if (viewPos.x >= screenBounds.x - objectWidth) 
        {
            isdartingBackwards = true;
            isdartingForward = false;
        } else if (viewPos.x <= screenBounds.x * -1 + (objectWidth * 2))
        {
            isdartingBackwards = false;
        }
        // stop player from moving outside of screen
        viewPos.x = Mathf.Clamp(viewPos.x, screenBounds.x * -1 + (objectWidth*2), screenBounds.x - objectWidth);
        viewPos.y = Mathf.Clamp(viewPos.y, screenBounds.y * -1 + objectHeight, screenBounds.y - objectHeight);
        transform.position = viewPos;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Food")
        {
            isdartingBackwards = true;
            isdartingForward = false;
        } else if (other.tag == "Enemy")
        {
            isDead = true;
            Time.timeScale = 0;
            gameController.updateSavedScore();

        }
    }

    public bool IsDarting()
    {
        // can only catch food if darting
        return (isdartingBackwards || isdartingForward);
    }
}
