﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodProximtyController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" && !other.transform.GetComponent<PlayerController>().IsDarting())
        {
            this.transform.parent.GetComponent<FoodController>().FlyAway();
        }
    }
}
