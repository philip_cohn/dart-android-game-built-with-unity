﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighscoresController : MonoBehaviour
{
    public Text highscoresText;


    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("highestScore") && highscoresText != null)
        {
            highscoresText.text = "Highscore: " + PlayerPrefs.GetInt("highestScore").ToString();

        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnApplicationQuit()
    {
        PlayerPrefs.Save();
    }
}
