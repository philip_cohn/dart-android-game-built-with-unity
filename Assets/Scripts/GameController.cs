﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController instance;         //A reference to our game control script so we can access it statically.
    public float scrollSpeed;
    public float maxSpeed;
    int food = 0;
    public Text foodText;
    private int backgroundsTraversed = 0;
    private EnemyPool enemyPool;

    void Awake()
    {
        //PlayerPrefs.DeleteAll();
        //If we don't currently have a game control...
        if (instance == null)
            //...set this one to be it...
            instance = this;
        //...otherwise...
        else if (instance != this)
            //...destroy this one because it is a duplicate.
            Destroy(gameObject);
    }

    void Start()
    {

        foodText.text = "Score: " + food;
        enemyPool = GetComponent<EnemyPool>();
    }

    public void AddFood(int foodValue)
    {
        food += foodValue;
        foodText.text = "Score: " + food;
    }

    public void AssessDifficulty()
    {
        backgroundsTraversed++;
        if (backgroundsTraversed > 1)
        {
            enemyPool.IncreaseDifficulty();
            if (scrollSpeed > maxSpeed)
            {
                scrollSpeed -= 0.5f;
            }
            backgroundsTraversed = 0;
        }
    }

    public void updateSavedScore()
    {
        if (PlayerPrefs.HasKey("highestScore"))
        {
            if (food > PlayerPrefs.GetInt("highestScore"))
            {
                PlayerPrefs.SetInt("highestScore", food);
            }
        } else
        {
            PlayerPrefs.SetInt("highestScore", food);
        }
    }

    void OnApplicationQuit()
    {
        PlayerPrefs.Save();
    }

}