﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodController : MonoBehaviour
{
    private GameController gameController;
    private bool isFlyingUp = false;
    private bool isFlyingDown = false;
    private float objectHeight;
    public ParticleSystem DestructionEffect;
    public int foodValue;
    public float flyingSpeed;
    public bool isActive = true; // only increment score if it passes through it while active 
    private Rigidbody2D rigidBody;
    private Camera MainCamera;
    private Vector2 screenBounds;



    // Start is called before the first frame update
    void Start()
    {
        gameController = FindObjectOfType<GameController>();
        rigidBody = GetComponent<Rigidbody2D>();
        MainCamera = FindObjectOfType<Camera>();
        objectHeight = transform.GetComponent<SpriteRenderer>().bounds.extents.y; //extents = size of height / 2
        screenBounds = MainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, MainCamera.transform.position.z));
    }

    void FixedUpdate()
    {
        if (isFlyingUp)
        {
            if (transform.position.y > screenBounds.y + objectHeight)
            {
                isFlyingUp = false;
                rigidBody.velocity = new Vector2(rigidBody.velocity.x, 0);
            } else
            {
                rigidBody.velocity = new Vector2(rigidBody.velocity.x, flyingSpeed);
            }
        }
        else if (isFlyingDown)
        {
            if (transform.position.y < screenBounds.y * -1 - (objectHeight * 2))
            {
                isFlyingDown = false;
                rigidBody.velocity = new Vector2(rigidBody.velocity.x, 0);
            } else
            {
                rigidBody.velocity = new Vector2(rigidBody.velocity.x, -flyingSpeed);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "Player" && isActive == true)
        {
            ParticleSystem explosionEffect = Instantiate(DestructionEffect)
                                             as ParticleSystem;
            explosionEffect.transform.position = transform.position;
            //play it
            explosionEffect.Play();

 

            gameController.AddFood(foodValue);
            setInactive();
        }
    }

    public void setActive()
    {
        isActive = true;
        gameObject.GetComponent<SpriteRenderer>().enabled = true;
    }

    public void setInactive()
    {
        isActive = false;
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
    }

    public void FlyAway()
    {
        if (transform.position.y > (screenBounds.y / 2))
        {
            isFlyingUp = true;
        } else  {
            isFlyingDown = true;
        }
    }

    public float getHeight()
    {
        return objectHeight;
    }
}