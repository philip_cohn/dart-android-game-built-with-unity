﻿using UnityEngine;
using System.Collections;

public class EnemyPool : MonoBehaviour
{
    public GameObject enemyPrefab;                                 //The enemy game object.
    public int enemyPoolSize;                                  //How many enemy to keep on standby.
    public int spawnLikelihood;
    public float spawnRate;                                    //How quickly enemy spawn.              
    public float enemySpeed = 4f;
    public float speedIncreaseStep;

    private GameObject[] enemy;                                   //Collection of pooled enemy.
    private int currentenemy = 0;                                  //Index of the current enemy in the collection.

    private Vector2 objectPoolPosition = new Vector2(-15, -25);     //A holding position for our unused enemy offscreen.
    private float spawnXPosition = 10f;
    public Camera MainCamera;
    private Vector2 screenBounds;

    private float timeSinceLastSpawned;


    void Start()
    {
        screenBounds = MainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, MainCamera.transform.position.z));

        timeSinceLastSpawned = 0f;

        //Initialize the enemy collection.
        enemy = new GameObject[enemyPoolSize];
        //Loop through the collection... 
        for (int i = 0; i < enemyPoolSize; i++)
        {
            //...and create the individual enemy.
            enemy[i] = (GameObject)Instantiate(enemyPrefab, objectPoolPosition, Quaternion.identity);
        }
    }


    //This spawns enemy as long as the game is not over.
    void FixedUpdate()
    {
        timeSinceLastSpawned += Time.deltaTime;

        if (timeSinceLastSpawned >= spawnRate)
        {
            timeSinceLastSpawned = 0f;

            if (Random.Range(0, 10) >= spawnLikelihood)
            {
                return;
            }



            //Set a random y position for the enemy
            float spawnYPosition = Random.Range(screenBounds.y * -1, screenBounds.y);

            //...then set the current enemy to that position.
            enemy[currentenemy].transform.position = new Vector2(spawnXPosition, spawnYPosition);

            enemy[currentenemy].GetComponent<EnemyController>().SetSpeed(enemySpeed);

            //Increase the value of currentenemy. If the new size is too big, set it back to zero
            currentenemy++;

            if (currentenemy >= enemyPoolSize)
            {
                currentenemy = 0;
            }
        }
    }

    public void IncreaseDifficulty()
    {

        if (spawnRate > 0.5f && enemySpeed <7)
        {
            if (spawnLikelihood < 5)
            {
                spawnLikelihood += 1;
            }
            enemySpeed += speedIncreaseStep;
            spawnRate -= .2f;
        }
    }
}
