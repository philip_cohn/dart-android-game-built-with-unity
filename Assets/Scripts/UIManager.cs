﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class UIManager : MonoBehaviour
{
    GameObject[] pauseObjects;
    GameObject[] finishObjects;
    GameObject[] tutorialObjects;
    GameObject[] firstTimePlayingObjects;


    PlayerController playerController;


    void Start()
    {

        Time.timeScale = 1;
        pauseObjects = GameObject.FindGameObjectsWithTag("ShowOnPause");
        finishObjects = GameObject.FindGameObjectsWithTag("ShowOnFinish");
        tutorialObjects = GameObject.FindGameObjectsWithTag("Tutorial");
        firstTimePlayingObjects = GameObject.FindGameObjectsWithTag("FirstTimePlaying");
        hidePaused();
        hideFinished();

        if (!PlayerPrefs.HasKey("playedBefore")) // if it's the first time playing, don't hide the tutorial screen
        {
            Time.timeScale = 0;
        } else
        {
            hideTutorial();
        }


        if (SceneManager.GetActiveScene().name == "Scene1")
        { 
            playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        }

    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.P) && !playerController.isDead)
        {
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
                showPaused();
            }
            else if (Time.timeScale == 0)
            {
                Time.timeScale = 1;
                hidePaused();
            }
        }

        if (Time.timeScale == 0 && playerController.isDead)
        {
            showFinished();
        }
}


    public void Reload()
    {
        SceneManager.LoadScene("Scene1");
    }

    public void pauseGame()
    {
        Time.timeScale = 0;
        showPaused();
    }

    public void unPauseGame()
    {
        Time.timeScale = 1;
        hidePaused();
    }

    public void showPaused()
    {
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(true);
        }
    }

    public void hidePaused()
    {
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(false);
        }
    }

    public void showFinished()
    {
        foreach (GameObject g in finishObjects)
        {
            g.SetActive(true);
        }
    }

    public void hideFinished()
    {
        foreach (GameObject g in finishObjects)
        {
            g.SetActive(false);
        }
    }

    private void hideTutorial()
    {
        foreach (GameObject g in tutorialObjects)
        {
            g.SetActive(false);
        }
    }

    public void goBackFromTutorial()
    {
        hideTutorial();
        showPaused();
    }

    public void playFromTutorial()
    {
        PlayerPrefs.SetInt("playedBefore", 1);
        hideTutorial();
        unPauseGame();
    }

    public void showTutorial()
    {
        hidePaused();
        hideFinished();
        foreach (GameObject g in tutorialObjects)
        {
            g.SetActive(true);
        }
        if (PlayerPrefs.HasKey("playedBefore"))
        {
            firstTimePlayingObjects[0].SetActive(false);
        }
    }



    //loads inputted level
    public void LoadLevel(string level)
    {
        SceneManager.LoadScene(level);
    }

    public void loadPrivacyPolicy()
    {
        Application.OpenURL("https://unity3d.com/legal/privacy-policy");
    }

    public void doExitGame()
    {
        PlayerPrefs.Save();
        Application.Quit();
    }
}
