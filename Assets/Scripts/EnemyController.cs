﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private float speed = 0;
    private Rigidbody2D rigidBody;
    private Camera MainCamera;
    private Vector2 screenBounds;
    private float objectWidth;
    private float objectHeight;
    private bool movingUp = true;

    // Use this for initialization
    void Start()
    {
        MainCamera = Camera.main;
        screenBounds = MainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, MainCamera.transform.position.z));
        objectWidth = transform.GetComponent<SpriteRenderer>().bounds.extents.x; 
        objectHeight = transform.GetComponent<SpriteRenderer>().bounds.extents.y; 
        rigidBody = GetComponent<Rigidbody2D>();
        transform.position = new Vector3(screenBounds.x * -1 + (objectWidth * 2), transform.position.y);
    }

    void FixedUpdate()
    {
        if (movingUp)
        {
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, speed);
            if (transform.position.y > screenBounds.y - objectHeight)
            {
                movingUp = false;
            }
        } else {
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, -speed);
            if (transform.position.y < screenBounds.y * -1 + objectHeight)
            {
                movingUp = true;
            }
        }
    }

    public void SetSpeed(float newSpeed)
    {
        speed = newSpeed;
    }

}
