﻿using UnityEngine;
using System.Collections;

public class FoodPool : MonoBehaviour
{
    public GameObject foodPrefabOne;                                 //The food game object.
    public GameObject foodPrefabTwo;                                 //The food game object.

    public int foodPoolSize = 8;                                  //How many food to keep on standby.
    public float spawnRate = 3f;                                    //How quickly food spawn.                                //Maximum y value of the food position.
    public int spawnLikelihood;

    private GameObject[] food;                                   //Collection of pooled food.
    private int currentfood = 0;                                  //Index of the current food in the collection.

    private Vector2 objectPoolPosition = new Vector2(-15, -25);     //A holding position for our unused food offscreen.
    private float spawnXPosition = 10f;
    public Camera MainCamera;
    private Vector2 screenBounds;
    private float foodHeight = 0;

    private float timeSinceLastSpawned;


    void Start()
    {
        screenBounds = MainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, MainCamera.transform.position.z));

        timeSinceLastSpawned = 0f;

        //Initialize the food collection.
        food = new GameObject[foodPoolSize];
        //Loop through the collection... 
        for (int i = 0; i < foodPoolSize; i++) 
        {
            if (i % 2 ==0)
            {
                food[i] = (GameObject)Instantiate(foodPrefabOne, objectPoolPosition, Quaternion.identity);
            } else
            {
                food[i] = (GameObject)Instantiate(foodPrefabTwo, objectPoolPosition, Quaternion.identity);
            }
        }


    } 


    //This spawns food as long as the game is not over.
    void Update()
    {
        timeSinceLastSpawned += Time.deltaTime;

        if ( timeSinceLastSpawned >= spawnRate)
        {
            timeSinceLastSpawned = 0f;
            if (Random.Range(0, 10) >= spawnLikelihood)
            {
                return;
            }

            if (foodHeight == 0) //if it hasn't been set yet (yeah i know hacky but eh)
            {
                foodHeight = food[0].GetComponent<FoodController>().getHeight() * 2;
            }

            //Set a random y position for the food
            float spawnYPosition = Random.Range((screenBounds.y * -1)+foodHeight, screenBounds.y-foodHeight);

            //...then set the current food to that position.
            food[currentfood].transform.position = new Vector2(spawnXPosition, spawnYPosition);

            food[currentfood].GetComponent<FoodController>().setActive();

            //Increase the value of currentfood. If the new size is too big, set it back to zero
            currentfood++;

            if (currentfood >= foodPoolSize)
            {
                currentfood = 0;
            }
        }
    }
}
